﻿using System.Diagnostics;
using System.Web.Hosting;

namespace VirtualPathExample.VirtuaPath
{

    public class VirtualRazorViewProvider : VirtualPathProvider
    {
        private readonly VirtualRazorViewsPathFactory razorRazorViewFactory;


        public VirtualRazorViewProvider(VirtualRazorViewsPathFactory razorRazorViewFactory)
        {
            this.razorRazorViewFactory = razorRazorViewFactory;
        }

        public override bool FileExists(string virtualPath)
        {
           
            VirtualRazorView razorView = this.GetFile(virtualPath) as VirtualRazorView;
            if (razorView != null)
                return true;

            return this.Previous.FileExists(virtualPath);
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            Debug.WriteLine(virtualPath);
            bool hasHandler = this.razorRazorViewFactory.HasHandlerFor(virtualPath);
            if (hasHandler)
            {
                return this.razorRazorViewFactory.GetFile(virtualPath);
            }
            return this.Previous.GetFile(virtualPath);
        }


    }
}