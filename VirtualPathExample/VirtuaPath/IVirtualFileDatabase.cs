﻿using System.Data.SqlClient;

namespace VirtualPathExample.VirtuaPath
{
    public interface IVirtualFileDatabase
    {
        string GetContent(string key);
        bool GetPathAvailability(string key);
    }

    public class VirtualFileDatabase : IVirtualFileDatabase
    {
        const string ConnectionString = @"Server=.\sqlexpress;Database=VirtualPath;Trusted_Connection=Yes;MultipleActiveResultSets=true";
        public string GetContent(string key)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT [CONTENT] FROM Views WHERE [Key]=@key",connection);
                cmd.Parameters.Add(new SqlParameter("key", key));
                return cmd.ExecuteScalar() as string;
            }

        }
        public bool GetPathAvailability(string key)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("SELECT count(1) FROM [Views] WHERE [Key] = @KEY",connection);
               
                cmd.Parameters.Add(new SqlParameter("@KEY", key));
                int items = (int)cmd.ExecuteScalar();
                return items > 0;
            }
        }
    }
}