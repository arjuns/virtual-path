﻿namespace VirtualPathExample.VirtuaPath
{
    public interface IVirtualImageFileHandler
    {
        bool CanHandle(string virtualFile);
        VirtualRazorView GetFile(string virtualFile);
    }
}