﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace VirtualPathExample.VirtuaPath
{
    public class VirtualRazorViewEngine : RazorViewEngine
    {
        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            RazorVirtualView view = new RazorVirtualView(controllerContext,
                partialViewName, null, false, null);
            ViewEngineResult result = new ViewEngineResult(view, this);
            return result;

            //return base.FindPartialView(controllerContext, partialViewName, useCache);
        }

    }
    public class RazorVirtualView : RazorView
    {
        
        public RazorVirtualView(ControllerContext controllerContext, string viewPath, string layoutPath, bool runViewStartPages, IEnumerable<string> viewStartFileExtensions)
            : base(controllerContext, viewPath, layoutPath, runViewStartPages, viewStartFileExtensions)
        {
        }

        public RazorVirtualView(ControllerContext controllerContext, string viewPath, string layoutPath, bool runViewStartPages, IEnumerable<string> viewStartFileExtensions, IViewPageActivator viewPageActivator)
            : base(controllerContext, viewPath, layoutPath, runViewStartPages, viewStartFileExtensions, viewPageActivator)
        {
        }
        protected override void RenderView(ViewContext viewContext,
            System.IO.TextWriter writer,
            object instance)
        {
            VirtualFileDatabase db=new VirtualFileDatabase();
            string content = db.GetContent(ViewPath);
            base.RenderView(viewContext, writer, instance);
        }
    }
}