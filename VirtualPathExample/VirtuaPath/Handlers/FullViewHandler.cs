﻿using System;

namespace VirtualPathExample.VirtuaPath.Handlers
{
    public class FullViewHandler : IVirtualImageFileHandler
    {
        private readonly IVirtualFileDatabase databse;
        public FullViewHandler()
        {
            databse = new VirtualFileDatabase();

        }
        public bool CanHandle(string virtualFile)
        {
            return databse.GetPathAvailability(virtualFile);

        }

        public VirtualRazorView GetFile(string virtualFile)
        {
            return new VirtualRazorView(virtualFile, databse.GetContent(virtualFile));

        }
    }

}