﻿using System;

namespace VirtualPathExample.VirtuaPath.Handlers
{
    public class PartialViewHandler : IVirtualImageFileHandler
    {
        public bool CanHandle(string virtualFile)
        {
            return false;
        }

        public VirtualRazorView GetFile(string virtualFile)
        {
            return null;
        }
    }
}