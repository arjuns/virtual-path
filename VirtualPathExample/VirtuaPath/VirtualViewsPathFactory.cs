﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using VirtualPathExample.VirtuaPath.Handlers;

namespace VirtualPathExample.VirtuaPath
{
    public class VirtualRazorViewsPathFactory
    {
        public List<IVirtualImageFileHandler> Handlers { get; set; }

        public VirtualRazorViewsPathFactory()
        {
            Handlers = new List<IVirtualImageFileHandler>();
            Handlers.Add(new FullViewHandler());
            Handlers.Add(new PartialViewHandler());
        }
        public VirtualFile GetFile(string virtualPath)
        {
            foreach (var handler in Handlers)
            {
                //First handler matching criterial would return file
                if (handler.CanHandle(virtualPath))
                    return handler.GetFile(virtualPath);
            }
            return null;
        }

        public bool HasHandlerFor(string virtualPath)
        {
            return this.Handlers.Any(handler => handler.CanHandle(virtualPath));
        }
    }
}