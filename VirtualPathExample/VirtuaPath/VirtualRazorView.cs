﻿using System.IO;
using System.Text;
using System.Web.Hosting;

namespace VirtualPathExample.VirtuaPath
{
    public class VirtualRazorView : VirtualFile
    {
        private readonly string content;

        public VirtualRazorView(string virtualPath, string content)
            : base(virtualPath)
        {
            this.content = content;
        }


        public override Stream Open()
        {

            return new MemoryStream(Encoding.UTF8.GetBytes(content));
            
            
        }
    }
}