﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace VirtualPathExample.Controllers
{
    public class HomeController : Controller
    {

        

        public ActionResult Index()
        {
            ViewBag.Message = "HACKED! Modify this template to jump-start your ASP.NET MVC application.";

            return View(new CarCollection());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

    public class  CarCollection:List<Car>
    {
        public CarCollection()
        {
            AddDefault();
        }

        private void AddDefault()
        {
            Car ferrari=new Car()
                {
                    Name = "Ferrari",Color = "Red",
                    Company = "Ferrari",
                    Price = 1000000.0M
                };
            Car tyota = new Car()
            {
                Name = "Tyota",
                Color = "Black",
                Company = "Toyta",
                Price = 10000.0M
            };
            this.Add(ferrari);
            this.Add(tyota);
        }
    }

    public class Car
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public string Company { get; set; }
        public decimal Price { get; set; }
    }
}
